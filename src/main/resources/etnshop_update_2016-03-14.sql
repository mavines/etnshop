alter table `product` add column serialNumber varchar(255) default null;

LOCK TABLES `product` WRITE;

update `product` set serialNumber = 'RR12313423CZ' where id = 1;
update `product` set serialNumber = 'RR50934534CZ' where id = 2;
update `product` set serialNumber = 'RR50935213CZ' where id = 3;

UNLOCK TABLES;
