package cz.etn.etnshop.utils;

/**
 * Created by mavines on 04/04/16.
 */
public class ProductSearch {
    private String searchTerm;

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }
}
