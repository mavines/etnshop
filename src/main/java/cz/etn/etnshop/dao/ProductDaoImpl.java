package cz.etn.etnshop.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.transform.ResultTransformer;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("productDao")
public class ProductDaoImpl extends AbstractDao implements ProductDao {

	@Override
	public void saveProduct(Product product) {
		persist(product);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Product> getProducts() {
		 Criteria criteria = getSession().createCriteria(Product.class);
	     return (List<Product>) criteria.list();
	}

	@Override
	public void deleteProduct(int productId) {
		Query query = getSession().createSQLQuery("delete from Product where id = :id");
        query.setInteger("id", productId);
        query.executeUpdate();
	}


	@Override
	public void updateProduct(Product product) {
		getSession().update(product);
		
	}

	@Override
	public Product getProduct(Integer id) {
		return (Product) getSession().get(Product.class, id);
	}

    @Override
    public List<Product> findProduct(String searchTerm) {
        final Query query = getSession().createSQLQuery("SELECT id,name,serialNumber FROM product "
                                                        + " WHERE MATCH (name,serialNumber) "
                                                        + " AGAINST ( :searchTerm IN NATURAL LANGUAGE MODE)");
        query.setString("searchTerm",searchTerm);
        query.setResultTransformer(new ResultTransformer() {
            @Override
            public Object transformTuple(Object[] tuple, String[] aliases) {
                final Product product = new Product();
                product.setId((Integer) tuple[0]);
                product.setName((String) tuple[1]);
                product.setSerialNumber((String) tuple[2]);
                return product;
            }

            @Override
            public List transformList(List collection) {
                return collection;
            }
        });
        return query.list();

    }

}
