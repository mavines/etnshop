package cz.etn.etnshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

import cz.etn.etnshop.dao.Product;
import cz.etn.etnshop.service.ProductService;
import cz.etn.etnshop.utils.ProductSearch;


@Controller
@RequestMapping("/product")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@RequestMapping(value = "/list",method = RequestMethod.GET)
	public ModelAndView list(ProductSearch search) {
		ModelAndView modelAndView = new ModelAndView("product/list");
                final List<Product> products = searchForProducts(search);
		System.out.println("Count:" + products.size());
		modelAndView.addObject("test", "mytest");
		modelAndView.addObject("count", products.size());
		modelAndView.addObject("products", products);
		modelAndView.addObject("productSearch", search);
	    return modelAndView;
	}

    private List<Product> searchForProducts(ProductSearch search) {
        return (search.getSearchTerm() == null || StringUtils.isEmpty(search.getSearchTerm())) ? productService.getProducts()
                                                                                                : productService.findProduct(search.getSearchTerm());
    }

    @RequestMapping("/add")
	public ModelAndView add(@RequestParam(value = "id", required = false) Integer id,
				Product newProduct ) {
		final Integer modifiedId = modifyProduct(newProduct);
		final ModelAndView modelAndView = new ModelAndView("product/add");
		if (modifiedId == null) {
			final Product existingProduct = (id == null) ? new Product() : productService.getProduct(id);
			modelAndView.addObject("newProduct", existingProduct);
		} else {
			modelAndView.addObject("newProduct", newProduct);
		}

	    return modelAndView;
	}

    private Integer modifyProduct(Product newProduct) {
        if (!newProduct.isEmpty()) {
            if (!newProduct.isNew()) {
                productService.updateProduct(newProduct);
            } else {
                productService.saveProduct(newProduct);
            }
            return newProduct.getId();
        }
        return null;
    }
}
