<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>etnShop</title>

    <spring:url value="/resources/core/css/hello.css" var="coreCss"/>
    <spring:url value="/resources/core/css/bootstrap.min.css"
                var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <link href="${coreCss}" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <h2>Products</h2>
    <table>
        <tr>
            <td>
                <form:form method="get" commandName="productSearch">
                    <form:input path="searchTerm"/>
                    <input type="submit" value="Search for products">
                </form:form>
            </td>
            <td>
                <a class="btn btn-link btn-lg" href="/etnshop/product/add" role="button">Add product</a>
            </td>
        </tr>
    </table>
    <table class="table">
        <thead>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Serial number</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${products}" var="product">
            <tr>
                <td><a href="/etnshop/product/add?id=${product.id}" role="link">${product.id}</a></td>
                <td><a href="/etnshop/product/add?id=${product.id}" role="link">${product.name}</a></td>
                <td>${product.serialNumber}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>

    <hr>
    <footer>
        <p>&copy; Etnetera a.s. 2015</p>
    </footer>
</div>

<spring:url value="/resources/core/css/bootstrap.min.js"
            var="bootstrapJs"/>

<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>