<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>etnShop</title>

    <spring:url value="/resources/core/css/hello.css" var="coreCss"/>
    <spring:url value="/resources/core/css/bootstrap.min.css"
                var="bootstrapCss"/>
    <link href="${bootstrapCss}" rel="stylesheet"/>
    <link href="${coreCss}" rel="stylesheet"/>
</head>
<body>
<div class="container">
    <h2>Add products</h2>
    <form:form commandName="newProduct" method="post">
        <form:hidden path="id"/>
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Serial number</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td><form:input path="name"/></td>
                <td><form:input path="serialNumber"/></td>
            </tr>
            </tbody>
        </table>
        <input type="submit" value="Add product"/>
    </form:form>
    <a class="btn btn-link btn-lg" href="/etnshop/product/list" role="button">List of products</a>
    <hr>
    <footer>
        <p>&copy; Etnetera a.s. 2015</p>
    </footer>
</div>

<spring:url value="/resources/core/css/bootstrap.min.js"
            var="bootstrapJs"/>

<script src="${bootstrapJs}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
</body>
</html>