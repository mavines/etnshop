# README #

### What is this repository for? ###

* Etnetera test task
* 1.0.0-SNAPSHOT

### How do I get set up? ###

* Required - Java SDK (1.7), Gradle, MySQL 
* Run SQL scripts from resources
* Spring MVC
* Check properties file in resources for proper auth, conection string
* Build project using gradle
* Run project using gradle

### Contribution guidelines ###

* Writing tests / for this stuff is not required I suppose
* Code review / Will be performed once for the whole project

### Who do I talk to? ###

* Maxim Nesen mavines@seznam.cz